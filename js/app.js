var routerApp = angular.module('routerApp', ['ui.router']);

routerApp.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/home',
            templateUrl: 'partial-home.html',
            controller: 'surveyController'
        })
        
        // nested list with custom controller
        .state('home.surveydetail', {
            url: '/:item',
            templateUrl: 'partial-home-detail.html',
            controller: 'detailController'
        })
        
        // nested list with just some random string data
        .state('home.paragraph', {
            url: '/paragraph',
            template: 'I could sure use a drink right now.'
        })
        
        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('about', {
            url: '/about',
            views: {
                '': { templateUrl: 'partial-about.html' },
                'columnOne@about': { template: 'Look I am a column!' },
                'columnTwo@about': { 
                    templateUrl: 'table-data.html',
                    controller: 'scotchController'
                }
            }
            
        });
        
});

routerApp.controller('surveyController', function($scope, $http) {

    $http.get('http://localhost/slim-test/surveys')
        .success(function(response) {
            $scope.surveys = response;
        })
        .error(function() {
            console.log('error')
        })

})

routerApp.controller('detailController', function($scope, $http, $stateParams) {
    $scope.dog = $stateParams.item;

    $http({method: 'GET',
        url: 'http://localhost/slim-test/getappinfo',
        params: {appname: $stateParams.item},
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
         }
    })
    .success(function(data) {
        $scope.ths = data.fields;
        console.log(data);
    })
    .error(function() {
        console.log('error');
    })

    $http({method: 'GET',
        url: 'http://localhost/slim-test/getsurvey',
        params: {surveyTitle: $stateParams.item},
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
         }
    })
    .success(function(response) {
        console.log(response);
        $scope.surveyData = response;
    })
    .error(function() {
        console.log('error');
    });



})

routerApp.controller('scotchController', function($scope) {
    
    $scope.message = 'test';
   
    $scope.scotches = [
        {
            name: 'Macallan 12',
            price: 50
        },
        {
            name: 'Chivas Regal Royal Salute',
            price: 10000
        },
        {
            name: 'Glenfiddich 1937',
            price: 20000
        }
    ];
    
});