var app = angular.module('surveyApp', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/surveylist');

	$stateProvider
		.state('surveylist', {
			url: '/surveylist',
			templateUrl: 'surveylist.html'
		})

		.state('surveylist.surveydetail', {
			url: '/:item',
			templateUrl: 'detail.html',
			controller: 'detailController'
		})
})

app.controller('surveyController', function($scope, $http) {

	$http.get('http://localhost/slim-test/surveys')
		.success(function(response) {
			$scope.surveys = response;
		})
		.error(function() {
			console.log('error')
		})

})

app.controller('detailController', function($scope, $stateParams, $http) {
	$scope.test = $stateParams.item;
	$http({method: 'GET',
		url: 'http://localhost/slim-test/getsurvey',
		params: {surveyTitle: $stateParams.item},
		headers: {
		 	'Content-Type': 'application/x-www-form-urlencoded'
         }
	})
	.success(function(response) {
		console.log(response);
	})
	.error(function() {
		console.log('error');
	});

	$http({method: 'GET',
		url: 'http://localhost/slim-test/getappinfo',
		params: {surveyTitle: $stateParams.item},
		headers: {
		 	'Content-Type': 'application/x-www-form-urlencoded'
         }
	})
	.success(function(data) {
		console.log(data);
	})
	.error(function() {
		console.log('error');
	})
})